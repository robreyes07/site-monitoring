import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import './assets/styles/main.scss'
import Donut from 'vue-css-donut-chart'
import 'vue-css-donut-chart/dist/vcdonut.css';

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  Donut,
  render: h => h(App)
}).$mount('#app')

Vue.use(Donut)
