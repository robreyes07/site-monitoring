import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: 'E-Bureauet Site Uptime Monitoring'
    }
  }
  
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
