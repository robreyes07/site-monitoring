<?php

    $formdata = json_decode(file_get_contents("php://input"), TRUE);

    if( !isset($formdata) && $_GET['action']  == 'get_alerts' ) 
    {
        get_alerts();
    }

    elseif( $formdata['action'] == 'dismiss_alert' ) 
    {
        include('./inc/pdo.php');

        $alert_id = $formdata['alert_id'];

        $sql = "UPDATE alerts SET dismissed=? WHERE id = $alert_id";

        $result = $pdo->prepare($sql)->execute([ 1 ]);

        header('Content-Type: application/json');
        echo json_encode([
            'name' => $formdata['alert_name'],
            'success' => $result
        ]);
        
    }

    elseif ( !empty($formdata['check_id']) ) {
        include('./inc/pdo.php');

        $post_data = [
            'check_id'          => $formdata['check_id'],
            'check_name'        => $formdata['check_name'],
            'description'       => $formdata['description'],
            'check_type'        => $formdata['check_type'],
            'current_state'     => $formdata['current_state'],
            'importance_level'  => $formdata['importance_level'],
            'dismissed'         => 0,
            'state_changed_timestamp' => $formdata['state_changed_timestamp']
        ];

        $sql = "INSERT INTO alerts (check_id, check_name, description, check_type, current_state, importance_level, dismissed, state_changed_timestamp) VALUES (:check_id, :check_name, :description, :check_type, :current_state, :importance_level, :dismissed, :state_changed_timestamp)";
        
        $posted = $pdo->prepare($sql)->execute($post_data);

        if ($posted)
        {
            $return = [
                'message' => 'Alert has been posted.',
                'status'  => $posted
            ];
        }

        else {
            $return = [
                'message' => 'There was an error processing your request.',
                'status' => 'error'
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($return);
    }

    function get_alerts()
    {   
        include('./inc/pdo.php');

        $sql = $pdo->query('SELECT * FROM alerts WHERE check_type = "TRANSACTION" AND dismissed = 0');
        $results = $sql->fetchAll();
        $alerts = [];

        foreach( $results as $key => $alert ) 
        {
            $alerts[$key] = [
                'id'                => $alert['id'],
                'check_id'          => $alert['check_id'],
                'description'       => $alert['description'],
                'check_name'        => $alert['check_name'],
                'check_type'        => $alert['check_type'],
                'current_state'     => $alert['current_state'],
                'importance_level'  => $alert['importance_level'],
                'dismissed'         => $alert['dismissed'],
                'state_changed_timestamp' => $alert['state_changed_timestamp']
            ];

        }

        header('Content-Type: application/json');
        echo json_encode($alerts);
    }

?>