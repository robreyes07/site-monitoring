<?php 
    error_reporting(0);

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "http://api.openweathermap.org/data/2.5/group?appid=ae191bc8ef42698d8657754663f4fcc8&id=1701668,2618425&units=metric");
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $response = json_decode(curl_exec($curl), true);
    if (isset($response['cod'])) 
    {
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    $countries = $response['list'];
    $weather_data = array();

    foreach( $countries as $key => $country )
    {
        $weather_data[$key] = array(
            'country_code' => $country['sys']['country'],
            'curr_time'    => get_currtime($country['sys']['country']),
            'temp'         => round($country['main']['temp']),
            'weather'      => $country['weather'][0]['main'],
            'day'         => date('l'),
            'date'         => date('M d, Y')
        );
    }

    header('Content-Type: application/json');
    echo json_encode($weather_data);
    
    function get_currtime($country)
    {
        $timezone = \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, $country);
        $time = new DateTime('now', new DateTimeZone($timezone[0]));
        $cur_time = $time->format('h:i A');

        return $cur_time;
    }
?>