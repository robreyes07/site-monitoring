<?php 

    $hostname   =  'localhost';
    $username   =   'root';
    $password   =   '';
    $dbname     =   'eb_monitoring';
    
    try 
    {    
        $pdo = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
    } 
    
    catch(PDOException $e) 
    {
        echo $e->getMessage();    
    }

?>