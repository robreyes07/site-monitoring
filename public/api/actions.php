<?php
    $formdata = json_decode(file_get_contents("php://input"), TRUE);

    if( !isset($formdata) )
    {
        echo 'Direct access is not allowed.';
        exit;
    }

    if( $formdata['action'] == 'toggle_pause' )
    {

        $fields = [
            'paused' => $formdata['pause'],
            'checkids' => $formdata['data']['id']
        ];

        $response = toggle_tracking($fields);

        header('Content-Type: application/json');

        if (isset($response['error'])) 
        {
            echo json_encode($response);
            exit;
        }

        $toggle_response = array(
            'response' => 'success',
            'site_name' => $formdata['data']['name'],
            'paused' => $formdata['pause']
         );

        echo json_encode($toggle_response);
    }

    function toggle_tracking($fields)
    {
        $ch = curl_init('https://api.pingdom.com/api/3.1/checks');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer HRq3DwE6RXIpSlP-Ha47ujEGJkiDe6mxACKc7ePtGba4LNl0-cu-NmTxN4KicpW6A2BOxyE"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));

        $response = json_decode(curl_exec($ch), true);

        curl_close($ch);

        return $response;
    }

?>