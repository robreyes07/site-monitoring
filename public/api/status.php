<?php
    error_reporting(0);

    $checksList = pingdom_query( array('type'=>'checks') );

    $downsites = render_card($checksList['checks'], 'down');
    $upsites = render_card($checksList['checks'], 'up');
    $pausedsites = render_card($checksList['checks'], 'paused');
    $unknownsites = render_card($checksList['checks'], 'unknown');

    $cards = array_merge($downsites, $pausedsites, $upsites, $unknownsites);

    header('Content-Type: application/json');
    echo json_encode($cards);

    function pingdom_query($query)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.pingdom.com/api/3.1/'.$query['type'].'/'.$query['id']);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer I_K2sT5b06HFm7VDJSUm54bFJ4jDZzyDKTvuzAnhdd3HrdHIYc1n5TsxZoCupjwT5WfFkOc"));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        $response = json_decode(curl_exec($curl), true);
        if (isset($response['error'])) 
        {
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }

        return $response;
    }

    function render_card($data, $status)
    {   

        usort($data, function($a, $b) {
            return $b['lasterrortime'] <=> $a['lasterrortime'];
        });

        $card_data = array();

        foreach($data as $cardlist) 
        {
            if($cardlist['status'] == $status)
            {   
                $average_res = get_average_response($cardlist['id']);

                $card = array(
                    'id'                => $cardlist['id'],
                    'name'              => $cardlist['name'],
                    'hostname'          => $cardlist['hostname'],
                    'uptime'            => get_uptime($cardlist['lasterrortime']),
                    'responsetime'      => $cardlist['status'] == 'up' ? $cardlist['lastresponsetime'] : '--',
                    'average'           => $average_res,
                    'status'            => ( $average_res['avgresponse'] >= 1500 ? 'notice' : $cardlist['status'] ),
                    'paused'            => $cardlist['status'] == 'paused' ? get_uptime($cardlist['lasttesttime']) : null
                );

                array_push($card_data, $card);
            }
        }

        return $card_data;
    }

    function get_average_response($id) 
    {
        $get_average = pingdom_query( array( 'type' => 'summary.performance', 'id' => $id ) );

        $recent = end($get_average['summary']['hours']);

        return $recent;
    }

    function get_maintenance_cards()
    {   
        $maintenance_cards = array();
        $paused_list = pingdom_query( array('type'=>'maintenance') );

        foreach($paused_list['maintenance'] as $paused_site)
        {
            if( $paused_site['effectiveto'] < strtotime('now') )
            {
                $card = array(
                    'id' => $paused_site['id'],
                    'checks' => $paused_site['checks']['uptime'],
                    'from' => $paused_site['from'],
                    'to' => $paused_site['effectiveto']
                );

                array_push($maintenance_cards, $card);
            }
        }

        return $maintenance_cards;
    }

    function get_mainte_details($id)
    {
        $query = pingdom_query(array('type'=>'maintenance', 'id' => $id));

        $details = array(
            'id'            => $query['maintenance']['id'],
            'description'   => $query['maintenance']['description'],
            'from'          => $query['maintenance']['from'],
            'to'            => $query['maintenance']['to'],
            'effectiveto'   => $query['maintenance']['effectiveto'],
            'paused'        => get_uptime($query['maintenance']['from'])
        );

        return $details;
    }

    function get_uptime($value)
    {
        $diff = abs(strtotime('now') - $value);

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $hours = floor(($diff - ($days * 86400)) / 3600);
        $minutes = round(abs($diff) / 60);

        if ( $years > 0 ) 
        {
            return '∞';
        }
        elseif ( $months > 0 )
        {
            if ( $months < 2 )
            {
                return $months.' mo';
            }
            else
            {
                return $months.' mos';
            }
        }
        elseif ( $days > 0 )
        {   
            if ( $days < 2 )
            {
                return $days.' day';
            }
            else
            {
                return $days.' days';
            }
            
        }
        elseif ( $hours > 0 )
        {   
            if ( $hours < 2 )
            {
                return $hours.' hr';
            }
            else
            {
                return $hours.' hrs';
            }
        }
        elseif ( $minutes > 0 )
        {
            if ( $minutes < 2 )
            {
                return $minutes.' min';
            }
            else
            {
                return $minutes.' mins';
            }
        }
        else 
        {
            return '--';
        }
    }
?>